db.Teams.insert({
	Team_ID: "eng1",
	Founded: new Date("Oct 04, 1896"),
    League: "Premier League",
	Points: 62,
	Team_Name: "Manchester United",
    Players: [ 	{ Player_ID: "Rooney", Goals: 85, Caps: 125, Age: 28 },
				{ Player_ID: "Scholes", Goals: 15, Caps: 225, Age: 28 },
				{ Player_ID: "Giggs", Goals: 45, Caps: 359, Age: 38 } ]
	});

db.Teams.insert({
	Team_ID: "eng2",
	Founded: new Date("Oct 04, 1899"),
    League: "Premier League",
	Points: 52,
	Team_Name: "Arsenal",
    Players: [ 	{ Player_ID: "Mata", Goals: 5, Caps: 24, Age: 27 },
				{ Player_ID: "Bergkamp", Goals: 95, Caps: 98, Age: 48 } ]
	});

db.Teams.insert({
	Team_ID: "eng3",
	Founded: new Date("Oct 04, 1912"),
    League: "Premier League",
	Points: 65,
	Team_Name: "Chelsea",
    Players: [ 	{ Player_ID: "Costa", Goals: 15, Caps: 25, Age: 28 },
				{ Player_ID: "Ivanov", Goals: 5, Caps: 84, Age: 28 },
				{ Player_ID: "Drogba", Goals: 105, Caps: 125, Age: 35 } ]
	 });

db.Teams.insert({
	Team_ID: "spa1",
	Founded: new Date("Oct 04, 1912"),
     League: "La Liga",
	 Points: 80,
	 Team_Name: "Barcelona",
     Players: [ { Player_ID: "Messi", Goals: 195, Caps: 189, Age: 30 },
              { Player_ID: "Valdes", Goals: 0, Caps: 158, Age: 27 },
			  { Player_ID: "Iniesta", Goals: 72, Caps: 25, Age: 31},
			  { Player_ID: "Pique", Goals: 9, Caps: 201, Age: 38 } ]
	 });

db.Teams.insert({
	Team_ID: "spa2",
	Founded: new Date("Nov 04, 1914"),
     League: "La Liga",
	 Points: 72,
	 Team_Name: "Real Madrid",
     Players: [ { Player_ID: "Ronaldo", Goals: 135, Caps: 134, Age: 28 },
				 { Player_ID: "Bale", Goals: 75, Caps: 45, Age: 27 },
				 { Player_ID: "Marcelo", Goals: 11, Caps: 25, Age: 31 },
              { Player_ID: "Benzema", Goals: 125, Caps: 95, Age: 22 } ]
	 });

db.Teams.insert({
	Team_ID: "spa3",
	Founded: new Date("Oct 04, 1912"),
     League: "La liga",
	 Points: 68,
	 Team_Name: "Valencia",
     Players: [ { Player_ID: "Martinez", Goals: 26, Caps: 54, Age: 21 },
              { Player_ID: "Aimar", Goals: 45, Caps: 105, Age: 29 } ]
	 });

db.Teams.insert({
	Team_ID: "ita1",
	Founded: new Date("Oct 04, 1922"),
     League: "Serie A",
	 Points: 69,
	 Team_Name: "Roma",
     Players: [ { Player_ID: "Totti", Goals: 198, Caps: 350, Age: 35 },
              { Player_ID: "De Rossi", Goals: 5, Caps: 210, Age: 30 },
			  { Player_ID: "Gervinho", Goals: 43, Caps: 57, Age: 24 } ]
	 });

db.Teams.insert({
	Team_ID: "ita2",
	Founded: new Date("Oct 04, 1899"),
     League: "Serie A",
	 Points: 52,
	 Team_Name: "Juventus",
     Players: [ { Player_ID: "Buffon", Goals: 0, Caps: 225, Age: 37 },
              { Player_ID: "Pirlo", Goals: 45, Caps: 199, Age: 35 },
			  { Player_ID: "Pogba", Goals: 21, Caps: 42, Age: 20 } ]
	 });

db.Teams.insert({
	Team_ID: "ita3",
	Founded: new Date("Oct 04, 1911"),
     League: "Serie A",
	 Points: 62,
	 Team_Name: "AC Milan",
     Players: [ { Player_ID: "Inzaghi", Goals: 115, Caps: 189, Age: 35 },
              { Player_ID: "Abbiati", Goals: 0, Caps: 84, Age: 24 },
			  { Player_ID: "Van Basten", Goals: 123, Caps: 104, Age: 35 } ]
	 });

db.Teams.insert({
	Team_ID: "ita4",
	Founded: new Date("Oct 04, 1902"),
     League: "Serie A",
	 Points: 71,
	 Team_Name: "Inter Milan",
     Players: [ { Player_ID: "Handanovic", Goals: 0, Caps: 51, Age: 29 },
              { Player_ID: "Cambiasso", Goals: 35, Caps: 176, Age: 35 },
			  { Player_ID: "Palacio", Goals: 78, Caps: 75, Age: 31 } ]
	 });

db.Teams.find()


//Q1
db.Teams.update(
{_id: ObjectId("5a2d3fc1d663829a523ce83d") },
{ $set: { Team_ID: "eng1",
	Founded: new Date("Oct 04, 1901")
 }
});

//Q2
db.Teams.update(
    {_id: ObjectId("5a2d3fc1d663829a523ce83d") },
    { $set: { Team_ID: "eng1",
    	Founded: new Date("Oct 04, 1896"),
         League: "Premier League",
    	 Points: 62,
    	 Team_Name: "Manchester United",
         Players: [ { Player_ID: "Rooney", Goals: 85, Caps: 125, Age: 28 },
                    { Player_ID: "Scholes", Goals: 15, Caps: 225, Age: 28 },
    			    { Player_ID: "Giggs", Goals: 45, Caps: 359, Age: 38 },
                   	{ Player_ID: "Keane", Goals: 44, Caps: 326, Age: 33 }
				  ]
			},
	});
db.Teams.update(
    {_id: ObjectId("5a2d3fc1d663829a523ce845") },
    { $set: { Team_ID: "ita3",
    Founded: new Date("Oct 04, 1911"),
     League: "Serie A",
	 Points: 62,
	 Team_Name: "AC Milan",
     Players: [ { Player_ID: "Inzaghi", Goals: 115, Caps: 189, Age: 35 },
				{ Player_ID: "Abbiati", Goals: 0, Caps: 84, Age: 24 },
				{ Player_ID: "Van Basten", Goals: 123, Caps: 104, Age: 35 },
				{ Player_ID: "Kaka", Goals: 32, Caps: 112, Age: 53 }
			  ]
			}
    });

//Q3
db.Teams.find().sort({Founded:-1}).limit(1).pretty()

//Q4
db.Teams.update( {_id : ObjectId("5a2d3fc1d663829a523ce841") } ,
                {$inc : {"Players.$.Goals" : 3} } ,
                false , true);

var Teams =  db.Teams.find( {_id : ObjectId("5a2d3fc1d663829a523ce841") });

Teams.forEach(function (setter) {
  for (var index in setter.Players) {
    db.Teams.update(
      { _id:  ObjectId("5a2d3fc1d663829a523ce841"),
        "Players.Goals": setter.Players[index].Goals
      },
      {$inc : {"Players.$.Goals" : 3} }
    );
  }
});

//Q5
function updateRecords( objectId ) {
  var Teams =  db.Teams.find( {_id : objectId });

  Teams.forEach(function (setter) {
    for (var index in setter.Players) {
      db.Teams.update(
        { _id:  objectId,
          "Players.Goals": setter.Players[index].Goals
        },
        {$set : {"Players.$.Caps" : Math.round(setter.Players[index].Caps * 0.1)} }
      );
    }
  });
}

cursor = db.Teams.find({League:"Serie A"});
while ( cursor.hasNext() ) {
  updateRecords(cursor.next()._id );
}

//Q6
cursor = db.Teams.find({Team_Name:'Barcelona'});
while(cursor.hasNext()) {
  db.Teams.update( {Team_Name: "Arsenal"},
   { $set: { "Points": cursor.next().Points } }  )
}

//Q7
db.Teams.aggregate( [
    { $unwind: "$Players" },
    {
      $match: {
          'Players.Age' : {
            $gt: 30
          }
       }
    },
    {
      $match: {
          "Players.Player_ID": {
          $regex : "es"
        }
      }
    }
]);

//Q8
db.Teams.aggregate([
  {
    $group: {
      _id: { $toLower: "$League" },
      total: {
        $sum: "$Points"
      },
      count: {
        $sum: 1
      }
    }
  },
  {
    $sort: {
      total: -1
    }
  }
 ]);

//Q9
db.Teams.aggregate([
  {
    $unwind : "$Players"
  	},
  {
    $group: {
      _id: { $toLower: "$Team_Name" },
      total: {
        $sum: "$Players.Goals"
      }
    }
  },
  {
    $sort: {
      total: -1
    }
  }
 ]);

//Q10
db.Teams.aggregate([
  {$unwind: "$Players" },
  { $group: { _id: "$_id", total: { $sum: "Players.$.Age" } } },
  { $sort: { total: -1 } }
 ]);

db.Teams.aggregate(
[
  {
    "$unwind" : "$Players"
  },
  {
    "$group" : {
      "_id" : {
        "Team_Name" : "$Team_Name",
        "Points" : "Points"
      },
      "faveFood" : {
        "$first" : "$Players"
      }
    }
  },
  {
    "$group" : {
      "_id" : "$Players.Team_Name",
      "height" : {
        "$max" : "$_id.Age"
      }
    }
  }
]);


db.Teams.aggregate([
  { $match: { Team_Name: "M" } },
  { $group: { _id: "$Team_ID", total: { $sum: "Players" } } },
  { $sort: { total: -1 } }
 ]);
