db.students.insert({
    name: 'Mary',
    surname: 'Murray',
    nationality: 'Irish',
    age: '45',
    exams:[{
      course_name: 'Databases',
      mark: 56,
      date: '10/10/2011',
      credits:10
      },
      {
        course_name: 'Maths',
        mark: 76,
        date: '09/11/2012',
        credits:5
      },
      {
        course_name: 'Programming',
        mark: 45,
        date: '02/07/2014',
        credits:15
      },
    ]
});

db.students.insert({
    name: 'Bill',
    surname: 'Bellichick',
    nationality: 'American',
    age: '32',
    exams:[{
      course_name: 'Databases',
      mark: 55,
      date: '10/10/2011',
      credits:10
      },
      {
        course_name: 'Maths',
        mark: 87,
        date: '09/11/2012',
        credits:5
      },
      {
        course_name: 'Programming',
        mark: 45,
        date: '10/10/2011',
        credits:15
      }
    ]
});

db.students.insert({
    name: 'Tom',
    surname: 'Brady',
    nationality: 'Canadian',
    age: '22',
    exams:[{
        course_name: 'Databases',
        mark: 34,
        date: '09/11/2012',
        credits:10
      },
      {
        course_name: 'Maths',
        mark: 56,
        date: '02/07/2014',
        credits:5
      }
    ]
});

db.students.insert({
    name: 'John',
    surname: 'Bale',
    nationality: 'English',
    age: '24',
    exams:[{
        course_name: 'Databases',
        mark: 71,
        date: '10/10/2011',
        credits:10
      },
      {
        course_name: 'Maths',
        mark: 88,
        date: '10/10/2011',
        credits:5
      },
      {
        course_name: 'Programming',
        mark: 95,
        date: '09/11/2012',
        credits:15
      },
    ]
});

/* Exerises */
// Find all the students that failed
db.students.find({'exams.mark': {'$lte' : 40}}).pretty()

// Find the number of people that passed each exam
db.students.find({'exams.mark': {$not: {'$lte' : 40}}}).count()

// Find the student with the highest average mark
  
