DROP TABLE Application;
DROP TABLE Student;
DROP TABLE Student_Location;
DROP TABLE Prior_School;
DROP TABLE Reference_table;
DROP TABLE Application_Reference;

create TABLE Application (
  application_id int PRIMARY KEY,
  application_year INT
);

CREATE TABLE Reference_table (
  ref_id int PRIMARY KEY,
  ref_name_institution VARCHAR(200) UNIQUE,
  ref_statement VARCHAR(500),
  school_id INT,
  FOREIGN KEY (school_id) REFERENCES Prior_School(school_id)
);

CREATE TABLE Application_Reference (
  application_ID int,
  ref_name_institution VARCHAR(200) UNIQUE,
  CONSTRAINT app_ref_jc PRIMARY KEY (application_ID, ref_name_institution),
  CONSTRAINT FK_app
      FOREIGN KEY (application_ID) REFERENCES Application(application_id),
  CONSTRAINT FK_refname
      FOREIGN KEY (ref_name_institution) REFERENCES ReferenceTable(ref_name_institution)
);

CREATE TABLE Student (
  student_id INT PRIMARY KEY ,
  student_name VARCHAR(50),
  application_id int UNIQUE,
  FOREIGN KEY (application_id) REFERENCES Application(application_id)
);

CREATE TABLE Student_Location(
  street VARCHAR(100),
  state VARCHAR(30),
  zipcode VARCHAR(7),
  student_id int UNIQUE,
  FOREIGN KEY (student_id) REFERENCES Student(student_id)
);

CREATE TABLE Prior_School(
  school_id INT PRIMARY KEY,
  school_addr VARCHAR(100),
  gpa NUMBER(2)
);